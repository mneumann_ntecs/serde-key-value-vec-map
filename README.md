# serde-key-value-vec-map

Deserialize maps or JSON objects in serde to structs that implement the
`FromKeyValue` trait.

## Example

```rust
use serde_key_value_vec_map::*;

#[derive(Debug)]
struct SingleMeasurement {
    name: String,
    value: u32,
}

impl KeyValueLike for SingleMeasurement {
    type Key = String;
    type Value = u32;
    fn from_key_value(key: Self::Key, value: Self::Value) -> Self {
        Self { name: key, value }
    }
    fn key(&self) -> &Self::Key { &self.name }
    fn value(&self) -> &Self::Value { &self.value }
}

let json = r#"
    {
        "temperature": 40,
        "pressure": 123
    }
"#;

#[derive(Deserialize)]
struct Container {
    #[serde(flatten)]
    #[serde(with = "serde_key_value_vec_map")]
    measurements: Vec<SingleMeasurement>,
}

let container: Container = serde_json::from_str(json).unwrap();

println!("{:?}", container.measurements);
```
