# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- ## Unreleased -->

## [0.1.0](https://gitlab.com/mneumann_ntecs/serde-key-value-vec-map/-/tree/v0.1.0) - 2021-01-11

**Features**

- Support serialization.

**Breaking Changes**

- Remove `KeyValueVecMap` wrapper type. Instead, you should use `#[serde(with = "serde_key_value_vec_map")]`.

- Change `FromKeyValue` trait into `KeyValueLike` and add `key` and `value`
  accessor required for serialization.

## [0.0.1](https://gitlab.com/mneumann_ntecs/serde-key-value-vec-map/-/tree/v0.0.1) - 2021-01-08

**Features**

- Initial realease.

